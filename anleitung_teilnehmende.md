# CoderDojo Kanufahrt

Ahoi. Im Sommer ist Kanufahren ein großer Spaß. Doch so langsam wird es herbstlich. Die Temperaturen sinken, es wird windiger und regnerischer. Damit ihr auch in den kühlen Jahreszeiten ohne Erkältungsgefahr eure paddeltechnik verbessern könnt, sorgen wir heute für eine Alternative.

Dazu nutzen wir die visuelle Programmiersprache <b>Scratch</b>. Wenn du Scratch heute zum ersten Mal benutzt oder du ein bisschen aus der Übung bist, schau dir das nächste Kapitel an. Dort schauen wir auf die Grundlagen von Scratch. Wenn du dich damit schon etwas auskennst, kannst du den ersten Schritt auch überspringen.

Lies dir in der Anleitung bitte immer einen Absatz nach dem anderen durch und mache erst weiter, wenn du mit der Aufgabe darin fertig bist oder du den Inhalt verstanden hast.

## Wie funktioniert Scratch?

Wenn Menschen miteinander reden wollen, müssen beide dieselbe Sprache sprechen, sonst verstehen sie sich nicht. Leider verstehen Computer weder Deutsch, noch Englisch oder Französisch. Möchten wir dem Computer eine Aufgabe stelle, müssen wir diese in einer Programmiersprache beschreiben. Scratch ist so eine Programmiersprache. 

In den meisten Programmiersprachen nutzt man Text, um die Aufgaben zu beschreiben. Scratch ist anders. Hier benutzen wir farbige Blöcke, die wie Legosteine ineinander gesteckt werden. Schauen wir uns das an. Rufe die Internetseite https://scratch.mit.edu/projects/editor/ auf. Solltest du dabei Probleme haben, bitte eine Mentorin oder einen Mentor um Hilfe.

Wenn alles funktioniert hat, sieht das so aus:
![Die Scratch-Oberfläche](assets/scratch_empty.png "Die Scratch-Oberfläche")

Es kann sein, dass dir die Oberfläche nicht auf Deutsch angezeigt wird. In diesem Fall klicke auf gitterartige Kugel in der oberen linken Ecke und wähle die richtige Sprache aus.

In dem großen weißen Bereich in der Mitte werden wir unser Programm entwickeln. Dazu können wir alle Blöcke nutzen, die sich an der linken Seite befinden. Schau dir die Liste in Ruhe an. Die verschiedenen Farben der Blöcke sagen uns, welche Aufgaben diese erfüllen können. Das gucken wir uns gleich noch genauer an. 

An der rechten, oberen Seite des Bildes siehst du ein weiteres, weißes Feld mit einer Katze darin. Darin passieren die Dinge, die du gleich programmieren wirst. 

Los geht's: Nimm den zweiten Block von oben (Den mit der Aufschrift "Drehe dich um 15 Grad"). Ziehe ihn auf die große weiße Fläche in der Mitte und lasse ihn dort wieder los. Klicke nun ein paar mal auf den Block und beobachte was passiert.

Und? Du hast gerade ein kleines Program geschrieben und ausgeführt. Nun können wir weitere Blöcke hinzufügen. Wenn sie zueinander passen, kannst du sie auch zusammenstecken. Versuche das mit dem blauen "Gehe zu Zufallsposition"-Block. Wenn du beide miteinander verbunden hast, sieht das so aus:

![Zwei verbundene Scratch-Blöcke](assets/scratch_2_blocks.png "Zwei verbundene Scratch-Blöcke")

Hast du das gesehen? Jedes mal, wenn du auf einen der Blöcke klickst, werden beide nacheinander ausgeführt. Die Katze dreht sich und geht dann zu einer zufälligen Position auf der Karte.

Scratch bietet sehr viele Blöcke. Es ist unmöglich, sich alle beim ersten Mal anzuschauen oder zu merken. Du kennst jetzt die wichtigsten Funktionen von Scratch und kannst die Kanufahrt starten.

## Ein fahrendes Kanu

Jetzt geht es los. Zuerst wollen wir ein Kanu bauen und damit über die nicht ganz so blaue und nicht ganz so weite See fahren. In diesem und allen weiteren Schritten gilt: Es gibt meist mehrere Wege, um eine Aufgabe zu lösen. Falls du keinen passenden Block findest oder ein anderes Problem hat, helfen dir unsere Mentorinnen und Mentoren.

Denk daran, deine Arbeit regelmäßig zu speichern. Sonst kannst du bei einem Versehen oder einem Fehler deinen Fortschritt verlieren. Klicke zum Speichern im oberen blauen Band auf "Datei" und dann auf "Auf deinem Computer speichern".

Nun wollen wir unser Kanu bauen. Dazu bewege deinen Mauszeiger unten rechts auf das Feld mit dem Katzenkopf und klicke auf den Pinsel. Versuche hier ein Kanu und dich darin aus der Vogelperspektive zu malen. Klicke auf den Pinsel am linken Rand, such dir eine Farbe aus und leg los. Wenn du fertig bist, klicke oben links auf den Reiter Skripte, um zur anderen Ansicht zurückzukehren.

Wenn du nicht gerne zeichnest, kannst du auch ein fertiges Bild von uns benutzen. Wenn dein Kanu etwas zu groß geraten ist, kannst du es über die Einstellungen "Größe" rechts in der Mitte verkleinern.

Nun möchten wir auch mit dem Kanu fahren. Klicke im kleinen grauen Fenster unten rechts auf das Kanu, dass du soeben gezeichnet hast, sodass es blau umrahmt wird. Suche dir nun die Blöcke, die du benötigst, um das Kanu mit den Pfeiltasten fahren zu lassen. Weiter unten findest du einen Hinweis. Versuch die Aufgabe erst ohne diesen zu lösen. Wenn du nicht weiter kommst, kannst du dort gerne nachschauen.

<i>Hinweis: Für die Fahrt geradeaus, nutze den blauen Block "gehe 10er Schritt" und für den Richtungswechsel die blauen Blöcke "drehe dich um 15Grad". Für die Steuerung mit den Pfeiltasten brauchst du den gelben Block "Wenn Taste Leertaste gedrückt wird". Hier kannst du jede Taste einstellen.</i>

Wenn dein Kanu fährt, dann drehe noch ein paar Runden über den weißen See, bevor es mit der nächsten Aufgabe weitergeht. Vergiss nicht, zwischendurch eine Pause zu machen.

## Ein Rundkurs

Auf einem leeren weißen See herumzudümpeln ist schon etwas langweilig, oder nicht? Lass uns nun einen Rundkurs bauen. Dazu fahre mit der Maus über das Fotosymbol unten rechts und wähle den Pinsel aus, um ein neues Bühnenbild zu malen.

Male nun einen kleinen Rundkurs aus der Vogelperspektive. Baue ein paar enge Stellen und Kurven ein, damit die Fahrt später auch eine Herausforderung wird. Auch hier haben wir bereits eines vorbereitet, falls du nicht malen möchtest. Falls du das Hintergrundbild selber malst, achte darauf, dass sich der Fluss farblich vom Hintergrund abhebt. Wenn du also blau für den Fluss verwendest, verwende es nicht mehr für den Hintergrund.

Wenn du das Bühnenbild fertig gemalt hast, wechsle wieder zur "Skripte"-Ansicht. Steuere nun dein Kanu über den neuen Hintergrund. Vielleicht fällt dir auf, dass du mit deinem Kanu nicht nur auf dem Wasser fahren kannst. Das soll so nicht sein.

Verändere dein Programm so, dass du, sobald du beim Kanufahren den Fluss verlässt, anhältst und an eine sichere Position zurückkehrst. Beispielsweise kannst du immer zu einem festen Startpunkt zurückkehren.

<i>Hinweis: Schau dir den hellblauen "Fühl"-Block "Wird Farbe berührt?" und die orangen "Steuerung"-Blöcke an.</i>

Wenn du das geschafft hast, teste deine Fahrkünste auf deinem Rundkurs.

Um diese Aufgabe zu lösen, musstest du wahrscheinlich mehrmals die gleichen Blöcke in der gleichen Abfolge verwenden. Das muss doch besser gehen? Mit Scratch kannst du auch rosafarbene "Eigene Blöcke" gestalten. Klicke dazu auf den rosafarbenen Kreis am linken Rand und dann auf "Neuer Block". Gib dem Block einen Namen und drücke ok.

Nun siehst du einen neuen rosafarbenen Block in der Bildmitte. Verschiebe alle Blöcke, die du brauchst, um eine Kollision mit dem Ufer zu prüfen, darunter. Ziehe nun vom linken Bildrand deinen neu erstellten Block in die Mitte und ersetze die Prüfung an allen Stellen durch deinen eigenen Block.

Das sieht doch schon übersichtlicher aus. Wenn du fertig bist, vergiss nicht zu speichern. Auf zur nächsten Aufgabe.

## Schatzsuche

Jetzt kommt ein wenig Spannung ins Spiel. Aus der gemütlichen Fahrt wird jetzt eine rasante Schatzsuche. Lege dazu ein neues Kostüm an. Falls du vergessen hast, wie das geht, sie nochmal in der ersten Aufgabe nach. Zeichne einen Schatz und kehre dann zur "Skripte"-Ansicht zurück.

Im grauen Feld rechts unten kannst du zwischen den verschiedenen Objekten umschalten. Klicke immer auf das Objekt, für welches du das Programm verändern möchtest. Das ausgewählte Objekt ist blau umrahmt. 

Schreibe ein Programm für den Schatz, sodass dieser immer dann, wenn er vom Kanu eingesammelt wird, einen neuen Platz auf dem Fluss einnimmt.

<i>Hinweise: Schaue dir einmal die gelben "Ereignis"-Blöcke an. Hiermit kannst du Nachrichten zwischen verschiedenen Kostümen verschicken.</i>

Das Spiel ist dir noch zu einfach? Dann lass uns jetzt ein bisschen Zeitdruck hinzufügen. Sorge dafür, dass das Spiel nach 30 Sekunden endet und das während dieser Zeit gezählt wird, wie oft du den Schatz eingesammelt hast. Zum Zählen der Punkte benötigst du eine Variable. Lege sie über das orange "Variablen"-Menü am linken Bildrand an. Sorge dafür, dass sich das Spiel mit einem Klick auf die grüne Flagge am oberen rechten Rand erneut starten lässt.

<i>Hinweis: Um die Zeit zu messen, benötigst du die Stoppuhr aus dem "Fühlen"-Menü. Außerdem gibt es ein spezielles Ereignis, welches beim Klick auf die grüne Flagge aktiviert wird.</i>

Wenn du fertig bist, vergiss nicht zu speichern.

## Konkurrenz um den Schatz

Du denkst, das wars schon? Falsch gedacht. Denn der hinterlistige Dr. Treibholz versucht dich aus seinem Fluss zu vertreiben. Eine Kollision mit ihm, ist nicht zu empfehlen.

Lege ein neues Kostüm an und zeichne einen Haufen Treibholz.

Verändere dein Programm so, dass das Kanu bei Berührung mit dem Treibholz zum Startpunkt zurückkehrt.

Sorge dafür, dass sich der Treibholzhaufen von alleine durch die Flusslandschaft bewegt. Überlege dir dazu ein paar Regeln, nach denen sich das Treibholz bewegen soll.

Wenn du es besonders schwierig möchtest, kannst du auch mehrere Treibholzhaufen auf die Flusslandschaft loslassen.